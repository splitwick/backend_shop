from django.core.management.base import BaseCommand

from products.models import Coupon, Transaction

from .upload_dummy_db import SimulateStore


class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        print("Stage 1...")
        print("Delete Transactions and Coupons")
        Coupon.objects.all().delete()
        Transaction.objects.all().delete()
        print("Stage 2...")
        print("Running Simulator - Creating coupons and transactions")
        simulator = SimulateStore()
        simulator.run()
