from django.core.management.base import BaseCommand
from users.models import Profile
from products.models import Category, Product, Review, Transaction, Coupon


class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        print(Profile.objects.all().delete())
        print(Category.objects.all().delete())
        print(Product.objects.all().delete())
        print(Review.objects.all().delete())
        print(Transaction.objects.all().delete())
        print(Coupon.objects.all().delete())
