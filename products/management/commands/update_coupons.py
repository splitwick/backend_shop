from django.core.management.base import BaseCommand
from users.models import Profile
from products.models import Category, Product, Review, Transaction, Coupon
from datetime import datetime
import pytz
from products.helpers import CLOSED_NOT_SUCCESS


class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        print(
            "Open coupons before update", Coupon.objects.filter(status="OPEN").count()
        )
        utc = pytz.utc
        coupons = Coupon.objects.filter(status="OPEN").filter(
            available_till__lt=datetime.now(utc)
        )
        coupons.update(status=CLOSED_NOT_SUCCESS)
        print("Open coupons after update", Coupon.objects.filter(status="OPEN").count())
