from django.core.management.base import BaseCommand
from django.contrib.auth.models import User
from users.models import Profile
from products.models import Category, Product, Review, Coupon, Transaction
import requests
import random
from randomuser import RandomUser
import lorem
from faker import Faker
import re
from django.utils.timezone import make_aware
from datetime import timedelta

PEXEL_KEY1 = "563492ad6f9170000100000155c9cc8f281148c27d9cfef9e772f862"
PEXEL_KEY2 = "563492ad6f917000010000019cca4ee08df2465a9509eb6c308b569d"

CATEGORY_NAMES = [
    "Speakers",
    "Microphones",
    "Headphones",
    "Cables",
    "Phone Accessories",
    "Phone Cases",
    "Power Banks",
    "Smartwatches",
]


class SimulateStore:
    # Create coupons and transactions

    # Coupon Status
    WAITING_TO_OPEN = "WAITING_TO_OPEN"  # before opening we may stack some nice coupons
    OPEN = "OPEN"  # coupon is open, people can join
    CLOSED_SUCCESS = "CLOSED_SUCCESS"  # people bought product
    CLOSED_NOT_SUCCESS = "CLOSED_NOT_SUCCESS"  # time expiredtes

    def __init__(self):
        self.fake = Faker()
        number_of_products = Product.objects.all().count()

        waiting_index = int(0.1 * number_of_products)
        open_index = int(0.7 * number_of_products)
        closed_success_index = int(0.1 * number_of_products)
        closed_not_success_index = int(0.1 * number_of_products)

        self.products = Product.objects.all().order_by("?")

        self.waiting_products = self.products[0:waiting_index]
        self.open_products = self.products[waiting_index : waiting_index + open_index]
        self.closed_success_products = self.products[
            waiting_index
            + open_index : waiting_index
            + open_index
            + closed_success_index
        ]
        self.closed_not_success_products = self.products[
            waiting_index
            + open_index
            + closed_success_index : waiting_index
            + open_index
            + closed_success_index
            + closed_not_success_index
        ]

    def run(self):
        self.create_waiting_coupons()
        self.create_open_coupons()
        self.create_closed_not_success_coupons()
        self.create_closed_success_coupons()

    # 1 Waiting and Non transaciton
    def create_waiting_coupons(self):
        print("Creating Waiting Coupons...")
        for product in self.waiting_products:
            self.create_coupon(self.WAITING_TO_OPEN, product)
        print("Done...")

    # 2 Open
    def create_open_coupons(self):
        print("Creating Open Coupons...")
        for product in self.open_products:
            coupon = self.create_coupon(self.OPEN, product)
            self.create_transactions(coupon, self.OPEN)
        print("Done...")

    # 3 Closed Success
    def create_closed_success_coupons(self):
        print("Creating Closed Cussess Coupons...")
        for product in self.closed_success_products:
            coupon = self.create_coupon(self.CLOSED_SUCCESS, product)
            self.create_transactions(coupon, self.CLOSED_SUCCESS)
        print("Done...")

    # 4 Closed NoT SUCCESS
    def create_closed_not_success_coupons(self):
        print("Creating Closed Not Cussess Coupons...")
        for product in self.closed_not_success_products:
            coupon = self.create_coupon(self.CLOSED_NOT_SUCCESS, product)
            self.create_transactions(coupon, self.CLOSED_NOT_SUCCESS)
        print("Done...")

    def create_coupon(self, coupon_status, product):
        # Dynamic function to create everycase of coupons
        coupon = Coupon()
        coupon.product = product
        coupon.status = coupon_status
        coupon.name = product.name
        coupon.amount_of_items = random.randrange(5, 101, 5)
        coupon.discount = random.randrange(5, 96, 5)

        if coupon_status == self.WAITING_TO_OPEN:
            # created and available_till = default
            coupon.save()
            return coupon

        random_date_from_past = self.fake.date_time_between(
            start_date="-21d", end_date="-5d"
        )
        coupon.created = make_aware(random_date_from_past)

        if coupon_status == self.OPEN:
            coupon.available_till = make_aware(
                random_date_from_past + timedelta(days=22)
            )

        elif (
            coupon_status == self.CLOSED_SUCCESS
            or coupon_status == self.CLOSED_NOT_SUCCESS
        ):
            # those are always dates from past
            coupon.available_till = make_aware(
                random_date_from_past + timedelta(days=4)
            )

        coupon.save()
        return coupon

    def create_transactions(self, coupon, type):
        num_of_items = coupon.amount_of_items
        fresh = random.randint(0, 10)
        num_to_take = 0
        if type == self.CLOSED_SUCCESS:
            num_to_take = num_of_items
        elif fresh == 0:
            num_to_take = 0
        else:
            num_to_take = num_of_items - random.randint(1, num_of_items // 2)
        profiles = Profile.objects.all().order_by("?")[:12]
        # there can be also Open transaction without people if 0 randomized
        for profile in profiles[:10]:
            current_num = random.randint(1, 10)
            if num_to_take - current_num > 0:
                self.create_transaction(coupon, profile, current_num)
                num_to_take -= current_num
            else:
                break
        # check additional user if we have to buy all the rest of items
        if type == self.CLOSED_SUCCESS and num_to_take > 0:
            self.create_transaction(coupon, profiles[11], num_to_take)

    def create_transaction(self, coupon, profile, amount):
        transaction = Transaction()
        start_date = coupon.created
        end_date = coupon.available_till
        transaction.created = make_aware(
            self.fake.date_time_between(start_date=start_date, end_date=end_date)
        )
        transaction.coupon = coupon
        transaction.profile = profile
        transaction.number_of_items = amount
        transaction.save()


class Command(BaseCommand):
    def create_user(self, user_data):
        user_obj, _ = User.objects.get_or_create(
            username=user_data["login"]["username"],
            defaults={
                "first_name": user_data["name"]["first"],
                "last_name": user_data["name"]["last"],
                "email": user_data["email"],
            },
        )
        user_obj.set_password("123")
        user_obj.save()
        # profile_obj = Profile.objects.create(user=user_obj)
        # signals.py create profile!
        profile_obj = user_obj.profile
        profile_obj.image_url = user_data["picture"]["large"]
        profile_obj.save()
        return profile_obj

    def create_users(self, number):
        users = RandomUser.generate_users(number)
        for user in users:
            new_user = self.create_user(user._data)

    def wipe_db(self):
        User.objects.all().delete()
        Category.objects.all().delete()
        Product.objects.all().delete()
        Review.objects.all().delete()
        Coupon.objects.all().delete()
        Transaction.objects.all().delete()

    def get_random_pic(self):
        while True:
            try:
                link = f"https://picsum.photos/id/{random.randint(1, 1000)}/500/500"
                r = requests.get(link)
                if r.status_code == 200:
                    return link
            except Exception as e:
                print("Some error: ", e)
                raise e

    def get_working_pics(self, query_name, amount):
        url = f"https://api.pexels.com/v1/search?query={query_name}&per_page={amount}&page=1"
        header = {"Authorization": PEXEL_KEY1}
        r = requests.get(url, headers=header)
        if r.status_code == 200:
            try:
                return [photo["src"]["medium"] for photo in r.json()["photos"]]
            except Exception as e:
                print(f"Something went wrong with {query_name} -> ", e)
                return [self.get_random_pic() for _ in range(amount)]
        else:
            header = {"Authorization": PEXEL_KEY2}
            r = requests.get(url, headers=header)
            if r.status_code == 200:
                try:
                    return [photo["src"]["medium"] for photo in r.json()["photos"]]
                except Exception as e:
                    print(f"Something went wrong with {query_name} -> ", e)
                    return [self.get_random_pic() for _ in range(amount)]

    def create_categories(self):
        for name in CATEGORY_NAMES:
            working_name = name.title()
            category = Category.objects.create(name=working_name)
            category.save()

    def random_product_name(self, category_name: str):
        final_name = ""
        words = set([x.title() for x in re.findall(r"[\w']+", lorem.text())])
        num_words = random.randint(1, 4)
        category_added = False
        add_category = random.randint(0, 1)
        while num_words:
            word = random.choice(list(words))
            words.remove(word)
            final_name += f" {word}"
            num_words -= 1
            if add_category == 1:
                if not category_added:
                    final_name += f" {category_name.title()}"
                    category_added = True
        return final_name

    def create_product(self, category, photo_url):
        product = Product()
        product.name = self.random_product_name(category.name)
        product.price = random.randint(5, 100)
        product.description = lorem.text()[:1500]
        product.photo = photo_url
        product.category = category
        product.save()

    def create_products(self):
        categories = Category.objects.all()
        for category in categories:
            num_of_products = random.randint(30, 50)
            pictures = self.get_working_pics(category.name[:-1], num_of_products)
            for picture in pictures:
                self.create_product(category, picture)

    def make_review(self, author, product):
        fake = Faker()
        product_review = Review()
        product_review.product = product
        aware_datetime = make_aware(
            fake.date_time_between(start_date="-1y", end_date="now")
        )
        product_review.date_posted = aware_datetime
        product_review.description = lorem.paragraph()
        product_review.profile = author
        product_review.rating = random.randint(1, 5)
        product_review.save()

    def create_reviews(self, number_of_users):
        i = 0
        for product in Product.objects.all():
            profiles = Profile.objects.all().order_by("?")[: random.randint(0, 10)]
            for profile in profiles:
                self.make_review(profile, product)
                i += 1
        print(f"Created {i} reviews.")

    def handle(self, *args, **kwargs):
        number_of_users = 50
        print("Stage 0...")
        print("Wiping current db...")
        self.wipe_db()
        print("Db wiped...")
        print("Stage 1...")
        print("Creating new users and profiles...")
        self.create_users(number_of_users)
        print(f"Created {number_of_users} new Users and Profiles")
        print("Creating categories...")
        self.create_categories()
        print("Creating products...")
        self.create_products()
        print("Creating reviews...")
        self.create_reviews(number_of_users)
        print("Done...")
        print("Stage 2...")
        print("Running Simulator")
        simulator = SimulateStore()
        simulator.run()
