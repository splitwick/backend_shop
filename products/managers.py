from django.db.models import (
    Manager,
    Sum,
    Q,
    Count,
    ExpressionWrapper,
    FloatField,
    F,
    QuerySet,
    IntegerField,
)
from datetime import datetime, timedelta
from django.utils import timezone
import pytz
from .helpers import OPEN


class PopularManager(Manager):
    UTC = pytz.utc
    OPEN_FILTER = {
        "status": OPEN,
        "available_till__lt": datetime.now(UTC) + timedelta(days=3),
    }

    # if less than 30% of items then Item is hot!
    RESERVED_FILTER = {"reserved_percentage__gt": 50}

    def get_queryset(self):
        return (
            super()
            .get_queryset()
            .filter(**self.OPEN_FILTER)
            .annotate(reserved_items=Sum("transactions__number_of_items"))
            .annotate(
                reserved_percentage=ExpressionWrapper(
                    ((F("reserved_items") * 100 / F("amount_of_items") * 100) / 100),
                    output_field=FloatField(),
                )
            )
            .filter(**self.RESERVED_FILTER)
            .order_by("-reserved_percentage")
        )
        # sometimes reserved_items is None which is equal to 0
        # remain_items=ExpressionWrapper(F('amount_of_items') - F('reserved_items'), output_field=IntegerField()))


class CouponQuerySet(QuerySet):
    def open_coupons(self):
        return (
            self.filter(status=OPEN)
            .annotate(reserved_items=Sum("transactions__number_of_items"))
            .annotate(
                remain_items=ExpressionWrapper(
                    F("amount_of_items") - F("reserved_items"),
                    output_field=IntegerField(),
                )
            )
        )
