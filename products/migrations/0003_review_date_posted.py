# Generated by Django 3.0.4 on 2020-03-19 15:40

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ("products", "0002_review"),
    ]

    operations = [
        migrations.AddField(
            model_name="review",
            name="date_posted",
            field=models.DateTimeField(default=django.utils.timezone.now),
        ),
    ]
