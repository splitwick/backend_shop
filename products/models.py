from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator
from users.models import Profile
from django.utils import timezone
from .managers import PopularManager, CouponQuerySet
from .helpers import WAITING_TO_OPEN, OPEN, CLOSED_NOT_SUCCESS, CLOSED_SUCCESS
import pytz
from datetime import datetime


class Category(models.Model):
    name = models.CharField(max_length=250)

    def __str__(self):
        return self.name


class Product(models.Model):
    photo = models.URLField(max_length=500)
    price = models.FloatField(validators=[MinValueValidator(0)])
    name = models.CharField(max_length=250)
    description = models.TextField(max_length=2000)
    category = models.ForeignKey(
        Category, on_delete=models.CASCADE, related_name="products"
    )

    def calculate_rating(self):
        sum_of_rating = 0
        amount_of_reviews = len(self.reviews.all())
        for review in self.reviews.all():
            sum_of_rating += review.rating
        try:
            return int(sum_of_rating / amount_of_reviews)
        except ZeroDivisionError:
            return 0

    def __str__(self):
        return self.name


class Review(models.Model):
    profile = models.ForeignKey(
        Profile, on_delete=models.CASCADE, related_name="reviews"
    )
    description = models.TextField(max_length=2000)
    rating = models.PositiveIntegerField(
        default=5, validators=[MinValueValidator(1), MaxValueValidator(5)]
    )
    product = models.ForeignKey(
        Product, on_delete=models.CASCADE, related_name="reviews"
    )
    date_posted = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return f"{self.profile.user.username} : {self.product.name} - {self.rating}/5"


class Coupon(models.Model):
    TYPE_CHOICES = (
        (WAITING_TO_OPEN, "WAITING_TO_OPEN"),
        (OPEN, "OPEN"),
        (CLOSED_SUCCESS, "CLOSED_SUCCESS"),
        (CLOSED_NOT_SUCCESS, "CLOSED_NOT_SUCCESS"),
    )

    status = models.CharField(
        max_length=250, choices=TYPE_CHOICES, default=WAITING_TO_OPEN
    )
    name = models.CharField(max_length=250)  # probably same as product
    product = models.ForeignKey(
        Product, on_delete=models.CASCADE, related_name="coupons"
    )
    amount_of_items = models.PositiveIntegerField(
        default=50, validators=[MinValueValidator(1), MaxValueValidator(1000)]
    )
    created = models.DateTimeField(default=timezone.now)
    available_till = models.DateTimeField(null=True, blank=True)
    discount = models.PositiveIntegerField(
        default=10, validators=[MinValueValidator(1), MaxValueValidator(99)]
    )  # in percentage

    # managers
    objects = CouponQuerySet.as_manager()
    popular = PopularManager()

    def get_coupon_product_price(self):
        return float((100 - self.discount) * self.product.price / 100)

    # TODO need to create update view to update status of coupons

    def update_current_coupon(self):
        utc = pytz.utc
        # if time expired
        if self.available_till < datetime.now(utc):
            if self.items_remain() == 0:
                self.status = CLOSED_SUCCESS
            else:
                self.status = CLOSED_NOT_SUCCESS
        elif self.items_remain() == 0:
            self.status = CLOSED_SUCCESS
        self.save()

    def items_remain(self):
        sum_of_items_in_transactions = 0
        for transaction in self.transactions.all():
            sum_of_items_in_transactions += transaction.number_of_items
        return self.amount_of_items - sum_of_items_in_transactions

    def get_reserved_items(self):
        sum_of_items_in_transactions = 0
        for transaction in self.transactions.all():
            sum_of_items_in_transactions += transaction.number_of_items
        return sum_of_items_in_transactions

    def __str__(self):
        return f"{self.name} : {self.status} : {self.created}"


class Transaction(models.Model):
    created = models.DateTimeField(default=timezone.now)
    coupon = models.ForeignKey(
        Coupon, on_delete=models.CASCADE, related_name="transactions"
    )
    profile = models.ForeignKey(
        Profile, on_delete=models.CASCADE, related_name="transactions"
    )
    number_of_items = models.PositiveIntegerField(default=10)

    # TODO: we have to check if number of items which user want to take is less than the amout remain

    def calculate_money(self):
        return float(
            (100 - self.coupon.discount)
            * self.number_of_items
            * self.coupon.product.price
            / 100
        )

    def calculate_money_without_discount(self):
        return float(self.number_of_items * self.coupon.product.price)

    def __str__(self):
        return f"{self.coupon.name} - {self.profile.user.username} - {self.number_of_items} items"


class Watch(models.Model):
    created = models.DateTimeField(default=timezone.now)
    coupon = models.ForeignKey(Coupon, on_delete=models.CASCADE, related_name="watches")
    profile = models.ForeignKey(
        Profile, on_delete=models.CASCADE, related_name="watches"
    )
    watching = models.BooleanField(default=True)

    def __str__(self):
        return f"{self.created} : {self.profile.user.username} : {self.watching}"
