from django.urls import path, include
from rest_framework.routers import DefaultRouter
from .views import (
    ProductViewSet,
    CategoryViewSet,
    ReviewViewSet,
    CouponViewSet,
    TransactionViewSet,
    PopularCouponsViewSet,
    OpenCouponViewSet,
    HandleCoupon,
    WatchViewSet,
    WatchCoupon,
)

router = DefaultRouter(trailing_slash=False)
router.register("products", ProductViewSet, basename="products")
router.register("categories", CategoryViewSet, basename="categories")
router.register("reviews", ReviewViewSet, basename="reviews")
router.register("watches", WatchViewSet, basename="watches")
router.register("coupons", CouponViewSet, basename="coupons")
router.register("transactions", TransactionViewSet, basename="transactions")
router.register("popularcoupons", PopularCouponsViewSet, basename="popularcoupons")
router.register("opencoupons", OpenCouponViewSet, basename="opencoupons")

urlpatterns = [
    path("", include(router.urls)),
    path("join-coupon", HandleCoupon.as_view()),
    path("watch-coupon", WatchCoupon.as_view()),
]

# HINT
# URL pattern: ^users/$ Name: 'user-list'
# URL pattern: ^users/{pk}/$ Name: 'user-detail'
# URL pattern: ^accounts/$ Name: 'account-list'
# URL pattern: ^accounts/{pk}/$ Name: 'account-detail'
