from django.shortcuts import render
from .serializers import (
    ProductSerializer,
    CategorySerializer,
    ReviewSerializer,
    SingleProductSerializer,
    CouponSerializer,
    TransactionSerializer,
    PopularCouponSerializer,
    SingleCouponSerializer,
    OpenCouponSerializer,
    WatchSerializer,
)
from .models import Product, Category, Review, Coupon, Transaction, Watch
from rest_framework.viewsets import ModelViewSet
from rest_framework.views import APIView
from rest_framework import exceptions
from rest_framework.response import Response
from django.shortcuts import get_object_or_404
from rest_framework.permissions import IsAuthenticated
from rest_framework import status
from datetime import datetime
import pytz
from products.helpers import OPEN
from django.shortcuts import get_object_or_404


# TODO READ_ONLY for viewsets - prevent deleting and creating data with requests without permisions


class ProductViewSet(ModelViewSet):
    serializer_class = ProductSerializer
    queryset = Product.objects.all()

    def retrieve(self, request, pk=None):
        queryset = Product.objects.all()
        product = get_object_or_404(queryset, pk=pk)
        serializer = SingleProductSerializer(product)
        return Response(serializer.data)


class CategoryViewSet(ModelViewSet):
    serializer_class = CategorySerializer
    queryset = Category.objects.all()


class ReviewViewSet(ModelViewSet):
    serializer_class = ReviewSerializer
    queryset = Review.objects.all()


class OpenCouponViewSet(ModelViewSet):
    serializer_class = OpenCouponSerializer
    queryset = Coupon.objects.open_coupons()
    filter_fields = {
        "product__price": ["gte", "lte"],
        "product__category__name": ["iexact"],
    }
    search_fields = ("product__name",)


class CouponViewSet(ModelViewSet):
    serializer_class = CouponSerializer
    queryset = Coupon.objects.all()

    def retrieve(self, request, pk=None):
        queryset = Coupon.objects.all()
        coupon = get_object_or_404(queryset, pk=pk)
        serializer = SingleCouponSerializer(coupon)
        return Response(serializer.data)

    filter_fields = (
        "status",
        "product__category__name",
        "product__category__id",
    )


class TransactionViewSet(ModelViewSet):
    serializer_class = TransactionSerializer
    queryset = Transaction.objects.all()

    filter_fields = (
        "coupon__status",
        "coupon__id",
    )


class WatchViewSet(ModelViewSet):
    serializer_class = WatchSerializer
    queryset = Watch.objects.all()


class PopularCouponsViewSet(ModelViewSet):
    queryset = Coupon.popular.all()
    serializer_class = PopularCouponSerializer


class HandleCoupon(APIView):
    permission_classes = (IsAuthenticated,)
    """
    POST request
    {'coupon_id': 2133, 'amount': 1}
    """

    def get(self, request):
        print("get test")
        return Response({"msg": "get done"})

    def validate(self, data):
        if data.get("coupon_id") is None:
            raise exceptions.ValidationError({"coupon_id variable is null"})
        if data.get("amount") is None:
            raise exceptions.ValidationError({"amount variable is null"})

    def can_join(self, coupon_obj, amount, user_obj):
        profile = user_obj.profile
        if coupon_obj.transactions.all().filter(profile=profile):
            print("Already in")
            return False
        utc = pytz.utc
        # if coupon is not open then it is wrong
        if coupon_obj.status != OPEN:
            print("Not open")
            return False
        # if expired but not updated yet
        if coupon_obj.available_till < datetime.now(utc):
            coupon_obj.update_current_coupon()
            print("Expired")
            return False
        if coupon_obj.items_remain() < int(amount):
            print("Item amount")
            return False

        return True

    def create_transaction(self, user_obj, coupon_obj, amount):
        transaction = Transaction()
        utc = pytz.utc
        transaction.created = datetime.now(utc)
        transaction.coupon = coupon_obj
        transaction.profile = user_obj.profile
        transaction.number_of_items = amount
        transaction.save()
        coupon_obj.update_current_coupon()
        return transaction

    def join_coupon(self, user_obj, coupon_obj, amount):
        result = self.create_transaction(user_obj, coupon_obj, amount)
        print(result)
        return "Created"

    def post(self, request):
        msg = None
        self.validate(request.data)
        coupon_id = request.data["coupon_id"]
        amount = request.data["amount"]
        user_obj = request.user
        coupon_obj = get_object_or_404(Coupon, id=coupon_id)

        if self.can_join(coupon_obj, amount, user_obj):
            msg = self.join_coupon(user_obj, coupon_obj, amount)
            return Response({"msg": msg})
        else:
            print("Can't join")
            return Response(status=status.HTTP_400_BAD_REQUEST)


class WatchCoupon(APIView):
    """
        POST request
        {'coupon_id': 2133}
    """

    permission_classes = (IsAuthenticated,)

    def validate(self, data):
        if data.get("coupon_id") is None:
            raise exceptions.ValidationError({"coupon_id variable is null"})

    def change_status_or_create(self, user_obj, coupon_obj):
        p, created = Watch.objects.get_or_create(
            coupon=coupon_obj, profile=user_obj.profile,
        )
        if not created:
            p.watching = not p.watching
            p.save()
            return "Updated", p
        return "Created", p

    def post(self, request):
        print(request.data)
        self.validate(request.data)
        coupon_id = request.data["coupon_id"]
        user_obj = request.user
        coupon_obj = get_object_or_404(Coupon, id=coupon_id)
        msg, watch_obj = self.change_status_or_create(user_obj, coupon_obj)
        serializer = WatchSerializer(watch_obj)
        print({"watchObject": serializer.data})
        return Response({"msg": msg, "watchObject": serializer.data})
