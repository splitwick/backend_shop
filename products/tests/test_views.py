from rest_framework.test import APITestCase
from django.contrib.auth.models import User
from users.models import Profile
from rest_framework import status
from products.models import Category, Product, Review, Coupon, Transaction, Watch
from products.serializers import (
    ProductSerializer,
    CategorySerializer,
    ReviewSerializer,
    SingleProductSerializer,
    CouponSerializer,
    TransactionSerializer,
    PopularCouponSerializer,
    SingleCouponSerializer,
    OpenCouponSerializer,
    WatchSerializer,
)
from django.utils.timezone import make_aware
from datetime import timedelta
from django.urls import reverse
from faker import Faker
from datetime import datetime
import pytz


class CategoriesTest(APITestCase):
    def setUp(self):
        self.cat1 = Category.objects.create(name="Category1")
        self.cat2 = Category.objects.create(name="Category2")

    def test_get_all_categories(self):
        response = self.client.get(reverse("categories-list"))
        categories = Category.objects.all()
        serializer = CategorySerializer(categories, many=True)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_valid_single_category(self):
        response = self.client.get(reverse("categories-detail", args=[self.cat1.pk]))
        cat1_obj = Category.objects.get(pk=self.cat1.id)
        serializer = CategorySerializer(cat1_obj)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_invalid_single_category(self):
        response = self.client.get(reverse("categories-detail", args=[10]))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_create_category(self):
        category = {"name": "testCategory"}
        response = self.client.post(reverse("categories-list"), data=category)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(category["name"], response.data["name"])
        self.assertEqual(3, Category.objects.all().count())


class ProductsAndReviewsTest(APITestCase):
    def setUp(self):
        self.user1 = User.objects.create(username="user1")
        user2 = User.objects.create(username="user2")
        category1 = Category.objects.create(name="Category1")
        category2 = Category.objects.create(name="Category2")
        self.product1 = Product.objects.create(
            photo="www.dummyurl1.com",
            price=123,
            name="product1",
            description="dummy description",
            category=category1,
        )
        Product.objects.create(
            photo="www.dummyurl2.com",
            price=99,
            name="product2",
            description="dummy description",
            category=category2,
        )
        self.review1 = Review.objects.create(
            profile=self.user1.profile,
            description="nice product",
            rating=5,
            product=self.product1,
        )
        Review.objects.create(
            profile=user2.profile,
            description="just ok",
            rating=3,
            product=self.product1,
        )

    def test_get_all_products(self):
        response = self.client.get(reverse("products-list"))
        products = Product.objects.all()
        serializer = ProductSerializer(products, many=True)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_valid_single_product(self):
        response = self.client.get(reverse("products-detail", args=[self.product1.pk]))
        product1_obj = Product.objects.get(pk=self.product1.id)
        serializer = SingleProductSerializer(product1_obj)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_invalid_single_category(self):
        response = self.client.get(reverse("products-detail", args=[10]))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_get_all_reviews(self):
        response = self.client.get(reverse("reviews-list"))
        reviews = Review.objects.all()
        serializer = ReviewSerializer(reviews, many=True)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_valid_single_review(self):
        response = self.client.get(reverse("reviews-detail", args=[self.review1.pk]))
        review_obj = Review.objects.get(pk=self.review1.id)
        serializer = ReviewSerializer(review_obj)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_invalid_single_review(self):
        response = self.client.get(reverse("reviews-detail", args=[10]))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_create_review(self):
        review = {
            "profile": self.user1.profile.id,
            "description": "testing",
            "rating": 5,
            "product": self.product1.id,
        }
        response = self.client.post(reverse("reviews-list"), data=review)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data["description"], "testing")
        self.assertEqual(3, Review.objects.all().count())

    def test_delete_review(self):
        response = self.client.delete(reverse("reviews-detail", args=[self.review1.id]))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(0, Review.objects.filter(id=self.review1.id).count())


class CouponAndTransactionAndWatchTest(APITestCase):
    # Coupon Status
    WAITING_TO_OPEN = "WAITING_TO_OPEN"  # before opening we may stack some nice coupons
    OPEN = "OPEN"  # coupon is open, people can join
    CLOSED_SUCCESS = "CLOSED_SUCCESS"  # people bought product
    CLOSED_NOT_SUCCESS = "CLOSED_NOT_SUCCESS"  # time expiredtes

    def create_coupon(self, coupon_status, product):
        # Dynamic function to create everycase of coupons
        coupon = Coupon()
        coupon.product = product
        coupon.status = coupon_status
        coupon.name = product.name
        coupon.amount_of_items = 10
        coupon.discount = 10

        if coupon_status == self.WAITING_TO_OPEN:
            # created and available_till = default
            coupon.save()
            return coupon

        random_date_from_past = self.fake.date_time_between(
            start_date="-21d", end_date="-5d"
        )
        coupon.created = make_aware(random_date_from_past)

        if coupon_status == self.OPEN:
            coupon.available_till = make_aware(
                random_date_from_past + timedelta(days=22)
            )

        elif (
            coupon_status == self.CLOSED_SUCCESS
            or coupon_status == self.CLOSED_NOT_SUCCESS
        ):
            # those are always dates from past
            coupon.available_till = make_aware(
                random_date_from_past + timedelta(days=4)
            )

        coupon.save()
        return coupon

    def create_waiting_coupon(self):
        self.create_coupon(self.WAITING_TO_OPEN, self.product)

    def create_open_coupon(self):
        coupon = self.create_coupon(self.OPEN, self.product)
        self.create_transaction(coupon, self.user1.profile, 2)
        self.create_transaction(coupon, self.user2.profile, 3)
        # used 5 of 10 items

    def create_closed_success_coupon(self):
        coupon = self.create_coupon(self.CLOSED_SUCCESS, self.product)
        self.create_transaction(coupon, self.user1.profile, 5)
        self.create_transaction(coupon, self.user2.profile, 5)

    def create_closed_not_success_coupon(self):
        coupon = self.create_coupon(self.CLOSED_NOT_SUCCESS, self.product)
        self.create_transaction(coupon, self.user1.profile, 5)
        self.create_transaction(coupon, self.user2.profile, 3)

    def create_transaction(self, coupon, profile, amount):
        transaction = Transaction()
        start_date = coupon.created
        end_date = coupon.available_till
        transaction.created = make_aware(
            self.fake.date_time_between(start_date=start_date, end_date=end_date)
        )
        transaction.coupon = coupon
        transaction.profile = profile
        transaction.number_of_items = amount
        transaction.save()

    def setUp(self):
        self.UTC = pytz.utc
        self.fake = Faker()
        self.user1 = User.objects.create(username="user1")
        self.user2 = User.objects.create(username="user2")
        self.category1 = Category.objects.create(name="Category1")
        self.product = Product.objects.create(
            photo="www.dummyurl1.com",
            price=100,
            name="product1",
            description="dummy description",
            category=self.category1,
        )
        self.create_waiting_coupon()
        self.create_open_coupon()
        self.create_closed_success_coupon()
        self.create_closed_not_success_coupon()

    def test_get_all_coupons(self):
        response = self.client.get(reverse("coupons-list"))
        coupons = Coupon.objects.all()
        serializer = CouponSerializer(coupons, many=True)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_valid_single_coupon(self):
        coupon = Coupon.objects.first()
        response = self.client.get(reverse("coupons-detail", args=[coupon.id]))
        serializer = SingleCouponSerializer(coupon)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_invalid_single_category(self):
        response = self.client.get(reverse("coupons-detail", args=[10]))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_get_all_transactions(self):
        response = self.client.get(reverse("transactions-list"))
        transactions = Transaction.objects.all()
        serializer = TransactionSerializer(transactions, many=True)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_filtered_transactions(self):
        response = self.client.get("/transactions?coupon__status=OPEN")
        transactions = Transaction.objects.filter(coupon__status=self.OPEN)
        serializer = TransactionSerializer(transactions, many=True)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_valid_single_transactions(self):
        transaction = Transaction.objects.first()
        response = self.client.get(
            reverse("transactions-detail", args=[transaction.id])
        )
        serializer = TransactionSerializer(transaction)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_invalid_single_transaction(self):
        response = self.client.get(reverse("transactions-detail", args=[10]))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_get_watch(self):
        coupon = Coupon.objects.filter(status=self.OPEN)
        self.assertEqual(1, coupon.count())
        coupon = coupon[0]
        Watch.objects.create(coupon=coupon, profile=self.user1.profile)
        Watch.objects.create(coupon=coupon, profile=self.user2.profile)

        response = self.client.get(reverse("watches-list"))
        watches = Watch.objects.all()
        serializer = WatchSerializer(watches, many=True)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


# TODO: join coupon
# TODO: watch coupon
# TODO: popularcoupons
# TODO: opencoupons
