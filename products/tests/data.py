from django.contrib.auth.models import User
from users.models import Profile
from products.models import Category, Product, Review, Coupon, Transaction
import random
from randomuser import RandomUser
import lorem
from faker import Faker
import re
from django.utils.timezone import make_aware

CATEGORY_NAMES = [
    "Speakers",
    "Microphones",
    "Headphones",
    "Cables",
    "Phone Accessories",
    "Phone Cases",
    "Power Banks",
    "Smartwatches",
]


class CreateTestData:
    def __init__(self, number_of_users, amount_of_products_per_category):

        print("Stage 1...")
        print("Creating new users and profiles...")
        self.create_users(number_of_users)
        print(f"Created {number_of_users} new Users and Profiles")
        print("Creating categories...")
        self.create_categories()
        print(f"Creating {amount_of_products_per_category} products per category...")
        amount_created = self.create_products(amount_of_products_per_category)
        print(f"Created {amount_created} products in total...")
        print("Creating reviews...")
        self.create_reviews(number_of_users)
        print("Done...")

    def create_user(self, user_data):
        user_obj, _ = User.objects.get_or_create(
            username=user_data["login"]["username"],
            defaults={
                "first_name": user_data["name"]["first"],
                "last_name": user_data["name"]["last"],
                "email": user_data["email"],
            },
        )
        user_obj.set_password("123")
        user_obj.save()
        # profile_obj = Profile.objects.create(user=user_obj)
        # signals.py create profile!
        profile_obj = user_obj.profile
        profile_obj.image_url = user_data["picture"]["large"]
        profile_obj.save()
        return profile_obj

    def create_users(self, number):
        users = RandomUser.generate_users(number)
        for user in users:
            new_user = self.create_user(user._data)

    def wipe_db(self):
        User.objects.all().delete()
        Category.objects.all().delete()
        Product.objects.all().delete()
        Review.objects.all().delete()
        Coupon.objects.all().delete()
        Transaction.objects.all().delete()

    def get_random_pic(self):
        return f"https://picsum.photos/id/{random.randint(1, 1000)}/500/500"

    def get_working_pics(self, amount):
        return [self.get_random_pic() for _ in range(amount)]

    def create_categories(self):
        for name in CATEGORY_NAMES:
            working_name = name.title()
            category = Category.objects.create(name=working_name)
            category.save()

    def random_product_name(self, category_name: str):
        final_name = ""
        words = set([x.title() for x in re.findall(r"[\w']+", lorem.text())])
        num_words = random.randint(1, 4)
        category_added = False
        add_category = random.randint(0, 1)
        while num_words:
            word = random.choice(list(words))
            words.remove(word)
            final_name += f" {word}"
            num_words -= 1
            if add_category == 1:
                if not category_added:
                    final_name += f" {category_name.title()}"
                    category_added = True
        return final_name

    def create_product(self, category, photo_url):
        product = Product()
        product.name = self.random_product_name(category.name)
        product.price = random.randint(5, 100)
        product.description = lorem.text()[:1500]
        product.photo = photo_url
        product.category = category
        product.save()

    def create_products(self, amount_of_products_per_category):
        categories = Category.objects.all()
        for category in categories:
            num_of_products = amount_of_products_per_category
            pictures = self.get_working_pics(num_of_products)
            for picture in pictures:
                self.create_product(category, picture)
        return len(categories) * amount_of_products_per_category

    def make_review(self, author, product):
        fake = Faker()
        product_review = Review()
        product_review.product = product
        aware_datetime = make_aware(
            fake.date_time_between(start_date="-1y", end_date="now")
        )
        product_review.date_posted = aware_datetime
        product_review.description = lorem.paragraph()
        product_review.profile = author
        product_review.rating = random.randint(1, 5)
        product_review.save()

    def create_reviews(self, number_of_users):
        max_reviews_per_product = 5 if number_of_users > 5 else number_of_users
        print(f"Creating {max_reviews_per_product} reviews per products...")
        i = 0
        for product in Product.objects.all():
            profiles = Profile.objects.all().order_by("?")[
                :max_reviews_per_product
            ]  # max 5
            for profile in profiles:
                self.make_review(profile, product)
                i += 1
        print(f"Created {i} reviews.")
