from django.contrib.auth.models import User
from users.models import Profile
from products.models import Category, Product, Review, Coupon, Transaction, Watch
from django.test import TestCase

from products.models import Category, Product
from products.tests.data import CreateTestData, CATEGORY_NAMES
from django.utils.timezone import make_aware
from datetime import timedelta
from faker import Faker
from datetime import datetime
import pytz


class SimpleCategoryTest(TestCase):
    """Test module for Category model"""

    def setUp(self):
        Category.objects.create(name="Category1")
        Category.objects.create(name="Category2")

    def test_get_categories(self):
        categories = Category.objects.all()
        self.assertEqual(2, len(categories))
        cat1 = Category.objects.get(name="Category1")
        self.assertEqual("Category1", cat1.name)

    def test_filter_categories(self):
        categories = Category.objects.filter(name="Category1")
        self.assertEqual(1, len(categories))
        categories = Category.objects.filter(name__icontains="Category")
        self.assertEqual(2, len(categories))

    def test_update_categories(self):
        category = Category.objects.get(name="Category1")
        category.name = "Category3"
        category.save()
        categories = Category.objects.filter(name="Category3")
        self.assertEqual(1, len(categories))
        self.assertEqual(categories[0].name, category.name)
        self.assertEqual(categories[0].name, "Category3")

    def test_delete_categories(self):
        Category.objects.all().delete()
        categories = Category.objects.count()
        self.assertEqual(0, categories)


class SimpleProductAndReviewTest(TestCase):
    """Test module for Product model"""

    def setUp(self):
        user1 = User.objects.create(username="user1")
        user2 = User.objects.create(username="user2")
        category1 = Category.objects.create(name="Category1")
        category2 = Category.objects.create(name="Category2")
        product1 = Product.objects.create(
            photo="www.dummyurl1.com",
            price=123,
            name="product1",
            description="dummy description",
            category=category1,
        )
        Product.objects.create(
            photo="www.dummyurl2.com",
            price=99,
            name="product2",
            description="dummy description",
            category=category2,
        )
        Review.objects.create(
            profile=user1.profile,
            description="nice product",
            rating=5,
            product=product1,
        )
        Review.objects.create(
            profile=user2.profile, description="just ok", rating=3, product=product1
        )

    def test_get_products(self):
        products = Product.objects.all()
        self.assertEqual(2, len(products))
        products = Product.objects.filter(category__name__icontains="category2")
        self.assertEqual(1, len(products))
        product = Product.objects.get(name="product1")
        self.assertEqual("product1", product.name)
        user1 = User.objects.get(username="user1")
        product = user1.profile.reviews.all()[0].product
        self.assertEqual("product1", product.name)

    def test_filter_products(self):
        products = Product.objects.filter(name__icontains="product1")
        self.assertEqual(1, len(products))
        products = Product.objects.filter(name__icontains="product")
        self.assertEqual(2, len(products))

    def test_models_on_products(self):
        product1 = Product.objects.get(name="product1")
        self.assertEqual(2, product1.reviews.all().count())
        category1 = Category.objects.get(name="Category1")
        self.assertEqual(1, category1.products.all().count())

    def test_update_products(self):
        category3 = Category.objects.create(name="NewCategory")
        Product.objects.all().update(category=category3)
        products = Product.objects.all()
        for product in products:
            self.assertEqual(product.category.name, "NewCategory")
        product = products[0]
        product.name = "newName"
        product.save()
        self.assertEqual(1, Product.objects.filter(name="newName").count())

    def test_calculate_rating(self):
        av_rating_of_product1 = 4  # 3 + 5 / 2
        product1 = Product.objects.get(name="product1")
        self.assertEqual(av_rating_of_product1, product1.calculate_rating())
        product2 = Product.objects.get(name="product2")
        self.assertEqual(0, product2.calculate_rating())

    def test_delete_products(self):
        Product.objects.all().delete()
        self.assertEqual(0, Product.objects.count())
        self.assertEqual(0, Review.objects.count())
        self.assertEqual(0, Category.objects.get(name="Category1").products.count())


class CategoryProfileProductReviewTest(TestCase):
    def setUp(self):
        self.amount_of_users = 20
        self.amount_of_products_per_category = 5
        CreateTestData(self.amount_of_users, self.amount_of_products_per_category)

    def test_amount_of_objects_in_db(self):
        amount_of_categories = len(CATEGORY_NAMES)
        self.assertEqual(amount_of_categories, Category.objects.count())
        self.assertEqual(self.amount_of_users, Profile.objects.count())
        self.assertEqual(self.amount_of_users, User.objects.count())
        self.assertEqual(200, Review.objects.count())
        self.assertEqual(
            self.amount_of_products_per_category * amount_of_categories,
            Product.objects.count(),
        )


class CouponAndTransactionTest(TestCase):
    # Coupon Status
    WAITING_TO_OPEN = "WAITING_TO_OPEN"  # before opening we may stack some nice coupons
    OPEN = "OPEN"  # coupon is open, people can join
    CLOSED_SUCCESS = "CLOSED_SUCCESS"  # people bought product
    CLOSED_NOT_SUCCESS = "CLOSED_NOT_SUCCESS"  # time expiredtes

    def create_coupon(self, coupon_status, product):
        # Dynamic function to create everycase of coupons
        coupon = Coupon()
        coupon.product = product
        coupon.status = coupon_status
        coupon.name = product.name
        coupon.amount_of_items = 10
        coupon.discount = 10

        if coupon_status == self.WAITING_TO_OPEN:
            # created and available_till = default
            coupon.save()
            return coupon

        random_date_from_past = self.fake.date_time_between(
            start_date="-21d", end_date="-5d"
        )
        coupon.created = make_aware(random_date_from_past)

        if coupon_status == self.OPEN:
            coupon.available_till = make_aware(
                random_date_from_past + timedelta(days=22)
            )

        elif (
            coupon_status == self.CLOSED_SUCCESS
            or coupon_status == self.CLOSED_NOT_SUCCESS
        ):
            # those are always dates from past
            coupon.available_till = make_aware(
                random_date_from_past + timedelta(days=4)
            )

        coupon.save()
        return coupon

    def create_waiting_coupon(self):
        self.create_coupon(self.WAITING_TO_OPEN, self.product)

    def create_open_coupon(self):
        coupon = self.create_coupon(self.OPEN, self.product)
        self.create_transaction(coupon, self.user1.profile, 2)
        self.create_transaction(coupon, self.user2.profile, 3)
        # used 5 of 10 items

    def create_closed_success_coupon(self):
        coupon = self.create_coupon(self.CLOSED_SUCCESS, self.product)
        self.create_transaction(coupon, self.user1.profile, 5)
        self.create_transaction(coupon, self.user2.profile, 5)

    def create_closed_not_success_coupon(self):
        coupon = self.create_coupon(self.CLOSED_NOT_SUCCESS, self.product)
        self.create_transaction(coupon, self.user1.profile, 5)
        self.create_transaction(coupon, self.user2.profile, 3)

    def create_transaction(self, coupon, profile, amount):
        transaction = Transaction()
        start_date = coupon.created
        end_date = coupon.available_till
        transaction.created = make_aware(
            self.fake.date_time_between(start_date=start_date, end_date=end_date)
        )
        transaction.coupon = coupon
        transaction.profile = profile
        transaction.number_of_items = amount
        transaction.save()

    def setUp(self):
        self.UTC = pytz.utc
        self.fake = Faker()
        self.user1 = User.objects.create(username="user1")
        self.user2 = User.objects.create(username="user2")
        self.category1 = Category.objects.create(name="Category1")
        self.product = Product.objects.create(
            photo="www.dummyurl1.com",
            price=100,
            name="product1",
            description="dummy description",
            category=self.category1,
        )
        self.create_waiting_coupon()
        self.create_open_coupon()
        self.create_closed_success_coupon()
        self.create_closed_not_success_coupon()

    def test_waiting_coupon(self):
        coupon = Coupon.objects.filter(status=self.WAITING_TO_OPEN)
        self.assertEqual(1, coupon.count())
        coupon = coupon[0]
        self.assertEqual(90, coupon.get_coupon_product_price())
        self.assertEqual(coupon.available_till, None)
        self.assertEqual(coupon.discount, 10)
        self.assertEqual(coupon.amount_of_items, 10)
        self.assertEqual(0, coupon.transactions.count())

    def test_open_coupon(self):
        coupon = Coupon.objects.filter(status=self.OPEN)
        self.assertEqual(1, coupon.count())
        coupon = coupon[0]
        self.assertTrue(coupon.available_till > datetime.now(self.UTC))
        self.assertTrue(coupon.created < datetime.now(self.UTC))
        self.assertEqual(coupon.items_remain(), 5)
        self.assertEqual(coupon.get_reserved_items(), 5)

        transactions = coupon.transactions.all()
        self.assertEqual(2, transactions.count())

        transaction1 = Transaction.objects.get(
            profile=self.user1.profile, coupon=coupon
        )
        self.assertEqual(transaction1.number_of_items, 2)
        self.assertEqual(180, transaction1.calculate_money())
        self.assertEqual(200, transaction1.calculate_money_without_discount())
        transaction2 = Transaction.objects.get(
            profile=self.user2.profile, coupon=coupon
        )
        self.assertEqual(transaction2.number_of_items, 3)

    def test_closed_success_coupon(self):
        coupon = Coupon.objects.filter(status=self.CLOSED_SUCCESS)
        self.assertEqual(1, coupon.count())
        coupon = coupon[0]
        self.assertTrue(coupon.available_till < datetime.now(self.UTC))
        self.assertTrue(coupon.created < datetime.now(self.UTC))
        self.assertEqual(coupon.items_remain(), 0)
        self.assertEqual(coupon.get_reserved_items(), 10)

        transactions = coupon.transactions.all()
        self.assertEqual(2, transactions.count())

    def test_closed_not_success_coupon(self):
        coupon = Coupon.objects.filter(status=self.CLOSED_NOT_SUCCESS)
        self.assertEqual(1, coupon.count())
        coupon = coupon[0]
        self.assertTrue(coupon.available_till < datetime.now(self.UTC))
        self.assertTrue(coupon.created < datetime.now(self.UTC))
        self.assertEqual(coupon.items_remain(), 2)
        self.assertEqual(coupon.get_reserved_items(), 8)

        transactions = coupon.transactions.all()
        self.assertEqual(2, transactions.count())

    def test_watch(self):
        coupon = Coupon.objects.filter(status=self.OPEN)
        self.assertEqual(1, coupon.count())
        coupon = coupon[0]
        Watch.objects.create(coupon=coupon, profile=self.user1.profile)
        self.assertEqual(1, self.user1.profile.watches.all().count())
        self.assertEqual(1, coupon.watches.all().count())
