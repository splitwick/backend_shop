from rest_framework.serializers import (
    ModelSerializer,
    SerializerMethodField,
    IntegerField,
    FloatField,
)
from .models import Product, Category, Review, Coupon, Transaction, Watch
from django.db.models import Sum


class CategorySerializer(ModelSerializer):
    class Meta:
        model = Category
        fields = (
            "id",
            "name",
        )


class SingleProductSerializer(ModelSerializer):
    rating = SerializerMethodField()
    reviews = SerializerMethodField()

    def get_reviews(self, obj):
        return [
            {
                "id": review.id,
                "profile": {
                    "id": review.profile.id,
                    "username": review.profile.user.username,
                    "first_name": review.profile.user.first_name,
                    "last_name": review.profile.user.last_name,
                    "email": review.profile.user.email,
                    "picture": review.profile.image_url,
                },
                "description": review.description,
                "rating": review.rating,
                "date_posted": review.date_posted,
            }
            for review in obj.reviews.all().order_by("-date_posted")
        ]

    def get_rating(self, obj):
        return obj.calculate_rating()

    class Meta:
        model = Product
        fields = (
            "id",
            "photo",
            "price",
            "name",
            "description",
            "category",
            "rating",
            "reviews",
        )


class PopularCouponSerializer(ModelSerializer):
    reserved_items = IntegerField()
    reserved_percentage = FloatField()
    product = SingleProductSerializer()
    coupon_product_price = SerializerMethodField()

    def get_coupon_product_price(self, obj):
        return obj.get_coupon_product_price()

    class Meta:
        model = Coupon
        fields = (
            "id",
            "status",
            "name",
            "product",
            "amount_of_items",
            "created",
            "available_till",
            "discount",
            "reserved_items",
            "reserved_percentage",
            "coupon_product_price",
        )


class ProductSerializer(ModelSerializer):
    class Meta:
        model = Product
        fields = (
            "id",
            "photo",
            "price",
            "name",
            "description",
            "category",
        )


class ReviewSerializer(ModelSerializer):
    class Meta:
        model = Review
        fields = (
            "id",
            "profile",
            "description",
            "rating",
            "product",
            "date_posted",
        )


class CouponSerializer(ModelSerializer):
    coupon_product_price = SerializerMethodField()

    def get_coupon_product_price(self, obj):
        return obj.get_coupon_product_price()

    class Meta:
        model = Coupon
        fields = (
            "id",
            "status",
            "name",
            "product",
            "amount_of_items",
            "created",
            "available_till",
            "discount",
            "coupon_product_price",
        )


class SingleCouponSerializer(ModelSerializer):
    product = SingleProductSerializer()
    reserved_items = SerializerMethodField()
    items_remain = SerializerMethodField()
    coupon_product_price = SerializerMethodField()

    def get_coupon_product_price(self, obj):
        return obj.get_coupon_product_price()

    def get_reserved_items(self, obj):
        return obj.get_reserved_items()

    def get_items_remain(self, obj):
        return obj.items_remain()

    class Meta:
        model = Coupon
        fields = (
            "id",
            "status",
            "name",
            "product",
            "amount_of_items",
            "created",
            "available_till",
            "discount",
            "reserved_items",
            "items_remain",
            "coupon_product_price",
        )


class TransactionSerializer(ModelSerializer):
    class Meta:
        model = Transaction
        fields = (
            "id",
            "created",
            "coupon",
            "profile",
            "number_of_items",
        )


class OpenCouponSerializer(ModelSerializer):
    reserved_items = IntegerField()
    remain_items = IntegerField()
    product = SingleProductSerializer()
    coupon_product_price = SerializerMethodField()

    def get_coupon_product_price(self, obj):
        return obj.get_coupon_product_price()

    class Meta:
        model = Coupon
        fields = (
            "id",
            "status",
            "name",
            "product",
            "amount_of_items",
            "created",
            "available_till",
            "discount",
            "reserved_items",
            "remain_items",
            "coupon_product_price",
        )


class WatchSerializer(ModelSerializer):
    coupon = SerializerMethodField()

    def get_coupon(self, obj):
        # it this way for frontend -> whenever we click on observe then it is result without reloading
        return {"id": obj.coupon_id}

    class Meta:
        model = Watch
        fields = ("id", "created", "coupon", "profile", "watching")
