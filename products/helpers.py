WAITING_TO_OPEN = "WAITING_TO_OPEN"  # before opening we may stack some nice coupons
OPEN = "OPEN"  # coupon is open, people can join
CLOSED_SUCCESS = "CLOSED_SUCCESS"  # people bought product
CLOSED_NOT_SUCCESS = "CLOSED_NOT_SUCCESS"  # time expired
