# SplitWick Backend REST API
SplitWick is a web application which allows people to split promotions and get products cheaper.

# A diagram of website's architecture.
![SplitwickDiagram](/uploads/6225ac174c62e6837944e901c7273657/SplitwickDiagram.png)

# Detail diagram of cloud architecture.
![DiagramArchi](https://user-images.githubusercontent.com/32365708/129243295-3d258311-1073-41d3-af70-a5e96522dd96.png)


# CI/CD Pipeline
![Screenshot_2021-01-10_at_19.27.33](/uploads/20c358e26f926b9c126177b342abf05e/Screenshot_2021-01-10_at_19.27.33.png)

# An entity-relationship model (ER model).
![SplitWick_Backend](/uploads/a2ef4c4e71ca50e72d60597ec47bdc90/SplitWick_Backend.png)



## Contents


1. [Running SplitWick REST API locally](#running)
1. [Usage](#usage)



## Running SplitWick REST API locally


### 1. Running locally
#### 1.1 Setup Python Virtual Environment
```buildoutcfg
python3 -m venv venv
. venv/bin/activate
pip3 install -r requirements.txt
```
#### 1.2 Run script
```
./manage.py migrate
./manage.py runserver
```

#### 1.3 Custom commands
Upload Dummy Database
```
./mange.py upload_dummy_db
```

Update coupons (if time expired then OPEN coupons need to be closed)
```
./mange.py upload_dummy_db
```

Recreate coupons (if all coupons are closed, it is good to recreate them)
```
./mange.py recreate_coupons
```

Wipe database
```
./mange.py wipe_db
```

## Usage
### Products App Endpoints
For more info check `products.serializers.py` / `products.urls.py` and `products.views.py`
#### Products

```
[GET] /products (list all products)
[GET, POST, PATCH, DELETE] /products/<id>

Content:
{
    "photo": "",
    "price": null,
    "name": "",
    "description": "",
    "category": null
} 
```

#### Reviews
```
[GET] /reviews 
[GET, POST, PATCH, DELETE] /reviews/<id>

Content
{
    "profile": null,
    "description": "",
    "rating": null,
    "product": null,
    "date_posted": null
}
```

#### Categories
```
[GET] /categories 
[GET, POST, PATCH, DELETE] /categories/<id>

Content
{
    "name": ""
}
```

#### Watches
```
[GET] /watches 
[GET, POST, PATCH, DELETE] /watches/<id>

Content
{
    "created": null,
    "profile": null,
    "watching": false
}
```

#### Coupons
```
[GET] /coupons 
[GET, POST, PATCH, DELETE] /coupons/<id>

Content
{
    "status": null,
    "name": "",
    "product": null,
    "amount_of_items": null,
    "created": null,
    "available_till": null,
    "discount": null
}
```

#### Popular Coupons
```
[GET] /popularcoupons
returns coupons (with extra fields) which fulfills the condition of popular coupon
```

#### Open Coupons
```
[GET] /opencoupons
returns OPEN coupons (with extra fields) 
```


#### Transactions
```
[GET] /transactions 
[GET, POST, PATCH, DELETE] /transactions/<id>
content
{
    "created": null,
    "coupon": null,
    "profile": null,
    "number_of_items": null
}
```

#### Join Coupon
join coupon if not joined before + creates transaction
```
[POST] /join-coupon 
Authentication required (Bearer Token)
{'coupon_id': 2133, 'amount': 1}
```


#### Watch Coupon
watch coupon or unwatch
```
[POST] /watch-coupon 
Authentication required (Bearer Token)
{'coupon_id': 2133}
```

### Users App Endpoints
For more info check `users.serializers.py` / `users.urls.py` and `users.views.py`

#### Register 
```
[POST] auth/users/
Content
{
"username": "newusername",
"email": "newemail",
"password": "newpassword",
}
```

#### Login  (create and get token)
```
[POST] auth/jwt/create/
Content
{
"username": "newusername",
"password": "newpassword",
}
Returns Token as response
```


#### My Data
Returns all information about user, transactions, joined coupons, watches
```
[GET] /my-data 
Authentication required (Bearer Token)

```


#### Update Me
```
[POST] /update-me 
Authentication required (Bearer Token)

Data:
    city: "Berlin"
    country: "United States"
    email: "felecia.payne@example.com"
    first_name: "felecia12"
    last_name: "payne12"
    phone: "123123123"
    zip: "12938"

```


#### Running tests

``` bash
# runs all tests
./manage.py test
```
