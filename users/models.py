from django.db import models
from django.contrib.auth.models import User
from splitwick.settings import COUNTRIES

COUNTRIES_CHOICES = [(x["name"], x["name"]) for x in COUNTRIES]


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    default_photo = "https://randomuser.me/api/portraits/men/1.jpg"
    image_url = models.URLField(
        default=default_photo, max_length=200, null=True, blank=True
    )
    country = models.CharField(
        choices=COUNTRIES_CHOICES, default="United States", max_length=100
    )
    city = models.CharField(max_length=70, blank=True, null=True)
    zip = models.CharField(max_length=70, blank=True, null=True)
    phone = models.CharField(max_length=15, blank=True, null=True)

    def __str__(self):
        return f"{self.user.username} Profile"
