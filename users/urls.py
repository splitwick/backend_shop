from django.contrib import admin
from django.urls import path, include
from .views import UserProfileListCreateView, MyProfileDataView, UpdateProfile
from rest_framework.routers import DefaultRouter

router = DefaultRouter(trailing_slash=False)
router.register("my-data", MyProfileDataView)

urlpatterns = [
    # gets all user profiles and create a new profile
    path("", include(router.urls)),
    path("update-me", UpdateProfile.as_view(), name="update-me"),
    path("all-profiles", UserProfileListCreateView.as_view(), name="all-profiles"),
]

# /accounts/*
# http://localhost:8000/auth/users/me <-- short data about me
