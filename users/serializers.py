from rest_framework import serializers
from .models import Profile


class ProfileSerializer(serializers.ModelSerializer):
    user = serializers.StringRelatedField(read_only=True)

    class Meta:
        model = Profile
        fields = "__all__"


class MyProfileDataSerializer(serializers.ModelSerializer):

    user = serializers.SerializerMethodField()
    reviews = serializers.SerializerMethodField()
    transactions = serializers.SerializerMethodField()
    watches = serializers.SerializerMethodField()

    def get_watches(self, obj):
        return [
            {
                "id": watch.id,
                "created": watch.created,
                "coupon": {
                    "id": watch.coupon_id,
                    "status": watch.coupon.status,
                    "name": watch.coupon.name,
                    "amount_of_items": watch.coupon.amount_of_items,
                    "created": watch.coupon.created,
                    "available_till": watch.coupon.available_till,
                    "reserved_items": watch.coupon.get_reserved_items(),
                    "discount": watch.coupon.discount,
                    "coupon_product_price": watch.coupon.get_coupon_product_price(),
                    "product": {
                        "id": watch.coupon.product.id,
                        "photo": watch.coupon.product.photo,
                        "price": watch.coupon.product.price,
                        "name": watch.coupon.product.name,
                        "description": watch.coupon.product.description,
                    },
                },
                "watching": watch.watching,
            }
            for watch in obj.watches.all()
        ]

    def get_transactions(self, obj):
        return [
            {
                "id": transaction.id,
                "created": transaction.created,
                "coupon": {
                    "id": transaction.coupon.id,
                    "status": transaction.coupon.status,
                    "name": transaction.coupon.name,
                    "amount_of_items": transaction.coupon.amount_of_items,
                    "created": transaction.coupon.created,
                    "available_till": transaction.coupon.available_till,
                    "reserved_items": transaction.coupon.get_reserved_items(),
                    "discount": transaction.coupon.discount,
                    "coupon_product_price": transaction.coupon.get_coupon_product_price(),
                    # TODO put it on manager and other serializers
                    "product": {
                        "id": transaction.coupon.product.id,
                        "photo": transaction.coupon.product.photo,
                        "price": transaction.coupon.product.price,
                        "name": transaction.coupon.product.name,
                        "description": transaction.coupon.product.description,
                    },
                },
                "number_of_items": transaction.number_of_items,
                "cost_with_discount": transaction.calculate_money(),
                "cost_without_discount": transaction.calculate_money_without_discount(),
            }
            for transaction in obj.transactions.all()
        ]

    def get_reviews(self, obj):
        return [
            {
                "id": review.id,
                "rating": review.rating,
                "date_posted": review.date_posted,
            }
            for review in obj.reviews.all()
        ]

    def get_user(self, obj):
        return {
            "username": obj.user.username,
            "first_name": obj.user.first_name,
            "last_name": obj.user.last_name,
            "email": obj.user.email,
        }

    class Meta:
        model = Profile
        fields = (
            "id",
            "user",
            "country",
            "city",
            "zip",
            "phone",
            "image_url",
            "reviews",
            "transactions",
            "watches",
        )
