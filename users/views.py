from rest_framework.generics import (
    ListCreateAPIView,
    RetrieveUpdateDestroyAPIView,
)
from rest_framework import viewsets
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated
from .models import Profile
from .serializers import ProfileSerializer, MyProfileDataSerializer
from rest_framework.response import Response


# Create your views here.


class UserProfileListCreateView(ListCreateAPIView):
    queryset = Profile.objects.all()
    serializer_class = ProfileSerializer
    permission_classes = [IsAuthenticated]

    def perform_create(self, serializer):
        user = self.request.user
        serializer.save(user=user)


# class UserProfileDetailView(RetrieveUpdateDestroyAPIView):
#     serializer_class = ProfileSerializer
#     permission_classes = [IsAuthenticated]


class MyProfileDataView(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated]
    serializer_class = MyProfileDataSerializer
    queryset = Profile.objects.all()

    def get_queryset(self):
        profile = self.request.user.profile
        return Profile.objects.all().filter(pk=profile.pk)


class UpdateProfile(APIView):
    """
    city: "asd"
    country: "United States"
    email: "felecia.payne@example.com"
    first_name: "felecia12"
    last_name: "payne12"
    phone: "123123123"
    zip: "aasd"
    """

    permission_classes = (IsAuthenticated,)

    # TODO improve logging -> more info as response what was updated

    def validate(self, data):
        # TODO validate if good info!!!!
        pass

    def update_user(self, user_obj, data):
        user_obj.email = data.get("email", "")
        user_obj.first_name = data.get("first_name", "")
        user_obj.last_name = data.get("last_name", "")
        user_obj.save()
        profile = user_obj.profile
        profile.city = data.get("city", "")
        profile.country = data.get("country", "")
        profile.phone = str(data.get("phone", ""))
        profile.zip = str(data.get("zip", ""))
        profile.save()
        return user_obj

    def post(self, request):
        print(request.data)
        self.validate(request.data)
        update_data = request.data
        user_obj = request.user
        user_obj = self.update_user(user_obj, update_data)
        serializer = MyProfileDataSerializer(user_obj.profile)
        return Response({"msg": "Success", "user": serializer.data})
