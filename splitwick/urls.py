from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path("admin/", admin.site.urls),
    path("", include("products.urls")),
    path("auth/", include("djoser.urls")),
    path("auth/", include("djoser.urls.jwt")),
    path("accounts/", include("users.urls")),
]
# https://djoser.readthedocs.io/en/latest/base_endpoints.html
# /auth/users -> Register new user
# /auth/users/me/ ->	retrieve/update the currently logged in user
# /auth/jwt/create/ -> create a JWT by passing a valid user in the post request to this endpoint
# /auth/jwt/refresh/ -> 	get a new JWT once the lifetime of the previously generated one expires
# /accounts/all-profiles ->  get all user profiles and create a new one
# /accounts/profile/id/ -> detail view of a user's profile
